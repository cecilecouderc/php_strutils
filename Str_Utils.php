<?php

class StrUtils {
private $str;

public function __construct ($string) {
    $this->str = $string ;
}

public function show () {
    return $this->str ;
}
public function bold(){
	return '<strong>' . $this->str . '</strong>';
}
public function italic(){
    return '<i>' . $this->str . '</i>';
}
public function underline(){
    return '<u>' . $this->str . '</u>';
}
public function capitalize(){
    return strtoupper($this->str);
}

public function uglify(){
    $this->str = $this->bold();
    $this->str = $this->italic();
    $this->str = $this->underline();
    return $this->str;
}
}
