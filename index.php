<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Php - strUtils</title>
</head>
<body>
    
<?php

include 'Str_Utils.php';

$myStr = new StrUtils('php is awesome !');

echo 'Bold : ' . $myStr->bold() . '<br/>' ;
echo 'Italic : ' . $myStr->italic()  . '<br/>' ;
echo 'Underline : ' . $myStr->underline() . '<br/>' ;
echo 'Capitalise : ' . $myStr->capitalize() . '<br/>';
echo 'Uglify : '
?>
<?= $myStr->uglify() ?>
</body>
</html>